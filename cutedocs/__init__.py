# cutedocs: documentation tool
# Copyright 2021, LavaTech and the cutedocs contributors
# SPDX-License-Identifier: AGPL-3.0-only
from .cli import cli

__all__ = ["cli"]
