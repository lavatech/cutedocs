# cutedocs: documentation tool
# Copyright 2021, LavaTech and the cutedocs contributors
# SPDX-License-Identifier: AGPL-3.0-only


from pathlib import Path

import click
from tomlkit import parse


@click.group()
@click.pass_context
def cli(ctx):

    # TODO: use XDG
    config_directory = Path.home() / ".config" / "cutedocs"
    config_directory.mkdir(exist_ok=True)

    ctx.config_path = config_directory / "cutedocs.toml"
    with open(ctx.config_path, "r") as config_file:
        ctx.cfg = parse(config_file.read())
        print(ctx.cfg)


@cli.command()
@click.pass_context
def see(ctx):
    cfg = ctx.parent.cfg

    current_infra_name = cfg["self"]["current_infra"]
    print("current infra project:", current_infra_name)

    current_infra = cfg["infras"][current_infra_name]
    print("infra", current_infra_name, "path", current_infra["repository_path"])


@cli.command()
@click.argument("hostname")
@click.pass_context
def what(ctx, hostname: str):
    cfg = ctx.parent.cfg
    print("fetch from hostname", hostname)
