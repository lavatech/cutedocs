# cutedocs: documentation tool
# Copyright 2021, LavaTech and the cutedocs contributors
# SPDX-License-Identifier: AGPL-3.0-only
from setuptools import setup

setup(
    name="cutedocs",
    version="0.1",
    py_modules=["cutedocs"],
    install_requires=[
        "click==7.1.2",
        "tomlkit==0.7.0",
    ],
    entry_points="""
        [console_scripts]
        cutedocs=cutedocs:cli
    """,
)
